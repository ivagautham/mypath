//
//  MapViewController.swift
//  MyPATH
//
//  Created by VA Gautham  on 9/20/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet var map_local : MKMapView!
    @IBOutlet var seg_route : UISegmentedControl!
    @IBOutlet var img_Info : UIImageView!
    
    var didCenterMap : Bool = false
    var allStops : NSArray!
    var allShape : NSArray!
    
    var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Setup map and location service
        centerMap()
        
        //init the file manager
        let fileManager = FileManager()
        allStops = fileManager.stopDetails()
        allShape = fileManager.shapeDetails()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    @IBAction func routeSegmentDidChange(sender: AnyObject) {
        map_local.removeOverlays(map_local.overlays)
        switch seg_route.selectedSegmentIndex
        {
        case 0:
            addAnnocationsRoutesForMap(map_local, key: 0)
            break
        case 2:
            addAnnocationsRoutesForMap(map_local, key: 5)
            break
        case 3:
            addAnnocationsRoutesForMap(map_local, key: 8)
            break
        case 1:
            addAnnocationsRoutesForMap(map_local, key: 11)
            break
        default:
            break
        }
    }
    
    @IBAction func hideInfoView(sener: AnyObject) {
        img_Info.hidden = !(img_Info.hidden)
    }
    
    func centerMap() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        CLLocationManager.locationServicesEnabled()
        
        locationManager.requestAlwaysAuthorization()
        
        locationManager.startUpdatingLocation()
        
        map_local = MKMapView();
        map_local.delegate = self
    }
    
    func addAnnocationsPinsForMap(mapView: MKMapView!) {
        
        for item in allStops {
            let currentStop = item as NSDictionary
            
            var lat: Double = currentStop["stop_lat"] as Double
            var lon: Double = currentStop["stop_lon"] as Double
            var currentStopCoord :CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, lon)
            
            var objectAnnotation = MKPointAnnotation()
            objectAnnotation.coordinate = currentStopCoord
            objectAnnotation.title = currentStop.valueForKey("stop_name") as NSString
            mapView.addAnnotation(objectAnnotation)
        }
        
    }
    
    func addAnnocationsRoutesForMap(mapView: MKMapView!, key :Double!) {
        var locations = [CLLocation]()
        for shape in allShape {
            let currentShape = shape as NSDictionary
            var shapeid: Double = currentShape["shape_id"] as Double
            if(shapeid == key) {
                let currentcoordinate = CLLocation(latitude: currentShape["shape_pt_lat"] as Double, longitude: currentShape["shape_pt_lon"] as Double)
                locations.append(currentcoordinate)
            }
        }
        var coordinates = locations.map({ (location: CLLocation) ->
            CLLocationCoordinate2D in
            return location.coordinate
        })
        var polyline = MKPolyline(coordinates: &coordinates,
            count: locations.count)
        polyline.title = (key as NSNumber).stringValue
        mapView.addOverlay(polyline)
    }
    
    func mapViewWillStartLoadingMap(mapView: MKMapView!) {
        if(!didCenterMap) {
            map_local = mapView
            var theSpan:MKCoordinateSpan = MKCoordinateSpanMake(0.318539424022021, 0.253271927906013)
            var myHome:CLLocationCoordinate2D = CLLocationCoordinate2DMake(40.7426017055206, -74.0815289815213)
            var theRegion:MKCoordinateRegion = MKCoordinateRegionMake(myHome, theSpan)
            
            mapView.setRegion(theRegion, animated: true)
            didCenterMap = true
            
            //add map location pins after adjusting map location
            addAnnocationsPinsForMap(mapView)
            seg_route.selectedSegmentIndex = 0
            addAnnocationsRoutesForMap(mapView, key: 0)
        }
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        
        if annotation is MKUserLocation {
            //return nil so map view draws "blue dot" for standard user location
            return nil
        }
        
        let reuseId = "pin"
        
        var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView!.canShowCallout = true
            pinView!.animatesDrop = true
            pinView!.pinColor = .Green
        }
        else {
            pinView!.annotation = annotation
        }
        
        return pinView
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKPolyline {
            
            var string = NSString(string: overlay.title!)
            
            var polylineRenderer = MKPolylineRenderer(overlay: overlay)
            if(string.doubleValue == 0) {
                polylineRenderer.strokeColor = UIColor(red: (231.0/255.0), green: (38.0/255.0), blue: (25.0/255.0), alpha: 1.0)
            }
            else if(string.doubleValue == 5) {
                polylineRenderer.strokeColor = UIColor(red: (15.0/255.0), green: (111.0/255.0), blue: (248.0/255.0), alpha: 1.0)
            }
            else if(string.doubleValue == 8) {
                polylineRenderer.strokeColor = UIColor(red: (244.0/255.0), green: (157.0/255.0), blue: (39.0/255.0), alpha: 1.0)
            }
            else if(string.doubleValue == 11) {
                polylineRenderer.strokeColor = UIColor(red: (85.0/255.0), green: (200.0/255.0), blue: (37.0/255.0), alpha: 1.0)
            }
            polylineRenderer.lineWidth = 6.5
            
            return polylineRenderer
        }
        
        return nil
    }
    func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool)
    {
        //println("\(mapView.region.center.latitude) && \(mapView.region.center.longitude)")
        //println("\(mapView.region.span.latitudeDelta) && \(mapView.region.span.longitudeDelta)")
    }
    
}