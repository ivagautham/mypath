//
//  SettingsViewController.swift
//  MyPATH
//
//  Created by VA Gautham  on 9/20/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet var settingsTableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}