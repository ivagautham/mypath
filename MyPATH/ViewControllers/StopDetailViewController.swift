//
//  StopDetailViewController.swift
//  MyPATH
//
//  Created by VA Gautham  on 10/22/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class StopDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, MKMapViewDelegate, CLLocationManagerDelegate
{
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    let fileManager = FileManager()
    var dbManager : DBManager!
    
    var allStops : NSArray!
    var routesAtStop : NSMutableArray!
    var connectionsArray : NSMutableArray!
    
    internal var currentStop : Stop!
    
    @IBOutlet var map_local : MKMapView!
    var didCenterMap : Bool = false
    var locationManager: CLLocationManager!
    
    @IBOutlet var tbl_PATH : UITableView!
    
    @IBOutlet var col_Stops : UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        automaticallyAdjustsScrollViewInsets = false;
        
        //init the file manager
        dbManager = appDelegate.dbManager as DBManager
        allStops = dbManager.stopDetails()
        
        //Setup map and location service
        centerMap()
        
        routesAtStop = NSMutableArray()
        routesAtStop.removeAllObjects()
        routesAtStop = dbManager.getAllRouteForStopID(currentStop.stop_id) as NSMutableArray
       
        connectionsArray = NSMutableArray()
        connectionsArray.removeAllObjects()
        connectionsArray = dbManager.getAllConnectionsForRouteArray(routesAtStop) as NSMutableArray
        connectionsArray.removeObject(currentStop)
        
        //col_Stops.dataSource = self
        col_Stops.delegate = self
        
        self.navigationItem.title = currentStop.stop_name as NSString
        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func assignCurrentStop(currStop : Stop)
    {
        currentStop = currStop as Stop
        println(currentStop.stop_name as NSString)
        self.navigationController?.navigationItem.title = (currentStop.stop_name as NSString)
        didCenterMap = false
        // mapViewWillStartLoadingMap(map_local)
    }
    
    func centerMap() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        CLLocationManager.locationServicesEnabled()
        
        locationManager.requestAlwaysAuthorization()
        
        locationManager.startUpdatingLocation()
        
        map_local = MKMapView();
        map_local.delegate = self
    }
    
    func mapViewWillStartLoadingMap(mapView: MKMapView!) {
        if(!didCenterMap) {
            map_local = mapView
            var theSpan:MKCoordinateSpan = MKCoordinateSpanMake(0.0269517231297058, 0.0567555003834599)
            var myHome:CLLocationCoordinate2D = CLLocationCoordinate2DMake(currentStop.stop_lat as Double, currentStop.stop_lon as Double)
            var theRegion:MKCoordinateRegion = MKCoordinateRegionMake(myHome, theSpan)
            
            mapView.setRegion(theRegion, animated: true)
            didCenterMap = true
            
            //add map location pins after adjusting map location
            addAnnocationsPinsForMap(mapView)
        }
    }
    
    func addAnnocationsPinsForMap(mapView: MKMapView!) {
        
        var lat: Double = currentStop.stop_lat as Double
        var lon: Double = currentStop.stop_lon as Double
        var currentStopCoord :CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, lon)
        
        var objectAnnotation = MKPointAnnotation()
        objectAnnotation.coordinate = currentStopCoord
        objectAnnotation.title = currentStop.stop_name as NSString
        mapView.addAnnotation(objectAnnotation)
        mapView.selectAnnotation(objectAnnotation, animated: true)
        
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        
        if annotation is MKUserLocation {
            //return nil so map view draws "blue dot" for standard user location
            return nil
        }
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView!.animatesDrop = true
            pinView!.pinColor = .Red
        }
        else {
            pinView!.annotation = annotation
        }
        pinView!.selected = true
        pinView!.canShowCallout = true
        return pinView
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 20.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        var aView : UIView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 20))
        aView.backgroundColor = UIColor(red: (0.0/255.0), green: (122.0/255.0), blue: (255.0/255.0), alpha: 1.0)
        var aLabel : UILabel = UILabel(frame: aView.frame)
        aLabel.backgroundColor = UIColor.clearColor()
        aLabel.textColor = UIColor.whiteColor()
        aLabel.font = UIFont.boldSystemFontOfSize(12.5)
        
        var sectionString : String = String("     Avaliable PATH trains")
        aLabel.text = sectionString
        
        aView.addSubview(aLabel)
        return aView
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return routesAtStop.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell : StopTableViewCell = tableView.dequeueReusableCellWithIdentifier("StopTableViewCell") as StopTableViewCell
        
        var currentRoute : Route = routesAtStop.objectAtIndex(indexPath.row) as Route
        cell.lbl_StopName.text = (currentRoute.route_name as NSString)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        //        let stopDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StopDetailViewController") as StopDetailViewController
        //
        var currentRoute : Route = routesAtStop.objectAtIndex(indexPath.row) as Route
        //        stopDetailViewController.assignCurrentStop(currentStop)
        //
        //        self.navigationController?.pushViewController(stopDetailViewController, animated: true)
        
    }
    
    func collectionView(collectionView: UICollectionView!, numberOfItemsInSection section: Int) -> Int {
        return connectionsArray.count
    }
    
    func collectionView(collectionView: UICollectionView!, cellForItemAtIndexPath indexPath: NSIndexPath!) -> UICollectionViewCell!
    {
        var cell : StopDetailCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("StopDetailCollectionViewCell", forIndexPath: indexPath) as StopDetailCollectionViewCell
        
        var currentStop : Stop = connectionsArray.objectAtIndex(indexPath.row) as Stop
        cell.lbl_StopName.text = currentStop.stop_name as NSString
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
    }
}
