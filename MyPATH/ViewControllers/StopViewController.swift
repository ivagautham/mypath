//
//  StopViewController.swift
//  MyPATH
//
//  Created by VA Gautham  on 9/20/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import UIKit

class StopViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    let fileManager = FileManager()
    var dbManager : DBManager!
    
    @IBOutlet var tbl_allStops: UITableView!
    var allStops : NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        automaticallyAdjustsScrollViewInsets = false;
        tbl_allStops.contentInset = UIEdgeInsetsZero;

        //init the file manager
        dbManager = appDelegate.dbManager as DBManager
        allStops = dbManager.stopDetails()
        tbl_allStops.reloadData()
        

        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return allStops.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 90.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell : StopTableViewCell = tableView.dequeueReusableCellWithIdentifier("StopTableViewCell") as StopTableViewCell
        var currentStop : Stop = allStops.objectAtIndex(indexPath.row) as Stop
        
        cell.lbl_StopName.text = (currentStop.stop_name as NSString)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        let stopDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StopDetailViewController") as StopDetailViewController

        var currentStop : Stop = allStops.objectAtIndex(indexPath.row) as Stop
        stopDetailViewController.assignCurrentStop(currentStop)
        
        self.navigationController?.pushViewController(stopDetailViewController, animated: true)

    }
}