//
//  HomeViewController.swift
//  MyPATH
//
//  Created by VA Gautham  on 9/20/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import UIKit
import CoreLocation

class HomeViewController: UIViewController, CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    var locationManager: CLLocationManager!
    var seenError : Bool = false
    //var locationFixAchieved : Bool = false
    var locationStatus : NSString = "Not Started"
    
    var allStops : NSArray!
    var nearestStop : Stop!
    
    var latestSchedule : NSMutableArray!
    
    var allRoutes : NSArray!
    var allTrips : NSArray!
    
    @IBOutlet var lbl_Clock : UILabel!
    @IBOutlet var lbl_Calander : UILabel!
    
    @IBOutlet var lbl_locationDisabled : UILabel!
    @IBOutlet var lbl_locationEnabled : UILabel!
    @IBOutlet var lbl_nearestPathStation : UILabel!
    @IBOutlet var lbl_nearestPathStationDistance : UILabel!
    @IBOutlet var tbl_latestSchedule : UITableView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    let fileManager = FileManager()
    var dbManager : DBManager!
    
    // MARK : - view Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib
        dbManager = appDelegate.dbManager as DBManager
        //setting up the clock
        updateClock()
        var timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("updateClock"), userInfo: nil, repeats: true)
        //setting up location service manager
        initLocationService()
        
        allStops = dbManager.stopDetails()
        
        allRoutes = dbManager.routeDetails()
        allTrips = dbManager.tripDetails()
        
        nearestStop = Stop()
        nearestStop = allStops.objectAtIndex(0) as Stop
        
        latestSchedule = NSMutableArray()
        latestSchedule.removeAllObjects()
        
        //get the latest train schedule
        updateLatestSchedule()
        var trainScheduleTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: Selector("updateLatestSchedule"), userInfo: nil, repeats: true)
        
        let logo = UIImage(named: "logo-status.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK : - custom Method(s)
    
    func initLocationService() {
        lbl_locationDisabled.hidden = true
        
        lbl_locationEnabled.hidden = false
        lbl_nearestPathStation.hidden = false
        lbl_nearestPathStationDistance.hidden = false
        
        lbl_nearestPathStation.text = "?"
        lbl_nearestPathStationDistance.text = "?"
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        CLLocationManager.locationServicesEnabled()
        
        locationManager.requestAlwaysAuthorization()
        
        locationManager.startUpdatingLocation()
    }
    
    func updateClock() {
        
        let timeZone = NSTimeZone(name: "EDT")
        
        let tf = NSDateFormatter()
        tf.timeZone = timeZone;
        tf.dateFormat = "hh:mm:ss a"
        let time = NSDate()
        let timeString = tf.stringFromDate(time)
        lbl_Clock.text = timeString
        
        
        let df = NSDateFormatter()
        df.timeZone = timeZone;
        df.dateFormat = "MM-dd-yyyy"
        let date = NSDate()
        let dateString = df.stringFromDate(date)
        lbl_Calander.text = dateString
    }
    
    
    func updateLatestSchedule() {
        
        latestSchedule.removeAllObjects()
        
        let timeZone = NSTimeZone(name: "EDT")
        
        let tf = NSDateFormatter()
        tf.timeZone = timeZone;
        tf.dateFormat = "HH:mm:ss"
        var timeNow = NSDate()
        let timeString = tf.stringFromDate(timeNow)
        timeNow = tf.dateFromString(timeString)!
        
        
        var negativeArray = NSMutableArray()
        negativeArray.removeAllObjects()
        
        var positiveArray = NSMutableArray()
        positiveArray.removeAllObjects()
        
        var nearestStopID : NSNumber = nearestStop.stop_id as NSNumber
        
        var scheduleForStop : NSArray = dbManager.getScheduleForStopID(nearestStopID) as NSArray
                
        for item in scheduleForStop {
            
            let currentSchedule = item as StopTime
            
            //var isLastStop : Bool = dbManager.checkIfLastStopfor(CurrentSchedule: currentSchedule)
            var isLastStop : Bool = false
            if(isLastStop == false) {
                var trainTimeString : NSString = currentSchedule.departure_time as NSString
                var trainTime : NSDate = tf.dateFromString(trainTimeString)!
                
                let dif : NSTimeInterval =  (trainTime.timeIntervalSinceDate(timeNow))/60 //converting to mins
                var requiredDictionary = NSMutableDictionary()
                requiredDictionary.setValue(dif, forKey: "time_diff")
                requiredDictionary.setValue(currentSchedule, forKey: "StopTime")
                
                if(dif > 0) {
                    positiveArray.addObject(requiredDictionary)
                }
                else {
                    negativeArray.addObject(requiredDictionary)
                }
            }
        }
        
        var descriptor: NSSortDescriptor = NSSortDescriptor(key: "time_diff", ascending: true)
        var positiveSortedResults: NSArray = positiveArray.sortedArrayUsingDescriptors([descriptor])
        var negativeSortedResults: NSArray = negativeArray.sortedArrayUsingDescriptors([descriptor])
        if(positiveSortedResults.count > 5) {
            latestSchedule.addObjectsFromArray(positiveSortedResults.subarrayWithRange(NSMakeRange(0, 5)))
        }
        else {
            latestSchedule.addObjectsFromArray(positiveSortedResults.subarrayWithRange(NSMakeRange(0, positiveSortedResults.count)))
            latestSchedule.addObjectsFromArray(negativeSortedResults.subarrayWithRange(NSMakeRange(0,(5 -  positiveSortedResults.count))))
        }
        tbl_latestSchedule.reloadData()
    }
    
    // MARK : - location Method(s)
    
    // Location Manager Delegate stuff
    // If failed
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        locationManager.stopUpdatingLocation()
        if ((error) != nil) {
            if (seenError == false) {
                seenError = true
            }
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        //if (locationFixAchieved == false) {
        //  locationFixAchieved = true
        var locationArray = locations as NSArray
        var locationObj = locationArray.lastObject as CLLocation
        var coord = locationObj.coordinate
        
        var nearestStation : NSMutableDictionary =  getNearestPathStation(coord);
        var nearestStationStop : Stop = nearestStation.valueForKey("stop") as Stop
        if(!(nearestStop.stop_id.isEqualToNumber(nearestStationStop.stop_id))) {
            nearestStop = nearestStationStop as Stop
            updateLatestSchedule()
        }
        
        var nearestStationString : NSString = nearestStationStop.stop_name as NSString
        var nearestDistanceString : Double  = nearestStation.valueForKey("stop_distance") as Double
        
        lbl_nearestPathStation.text = nearestStationString
        lbl_nearestPathStationDistance.text = NSString(format: "%.2f mile(s)", nearestDistanceString)
        
        //}
    }
    
    // authorization status
    func locationManager(manager: CLLocationManager!,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var shouldIAllow = false
            
            switch status {
            case CLAuthorizationStatus.Restricted:
                locationStatus = "Restricted Access to location"
            case CLAuthorizationStatus.Denied:
                locationStatus = "User denied access to location"
            case CLAuthorizationStatus.NotDetermined:
                locationStatus = "Status not determined"
            default:
                locationStatus = "Allowed to location Access"
                shouldIAllow = true
            }
            NSNotificationCenter.defaultCenter().postNotificationName("LabelHasbeenUpdated", object: nil)
            if (shouldIAllow == true) {
                NSLog("Location to Allowed")
                // Start location services
                if(lbl_locationDisabled.hidden == false) {
                    lbl_locationDisabled.hidden = true
                    
                    lbl_locationEnabled.hidden = false
                    lbl_nearestPathStation.hidden = false
                    lbl_nearestPathStationDistance.hidden = false
                }
                
                locationManager.startUpdatingLocation()
            } else {
                lbl_locationDisabled.hidden = false
                
                lbl_locationEnabled.hidden = true
                lbl_nearestPathStation.hidden = true
                lbl_nearestPathStationDistance.hidden = true
                
                NSLog("Denied access: \(locationStatus)")
            }
    }
    
    func getNearestPathStation(coord: CLLocationCoordinate2D!) -> NSMutableDictionary
    {
        //getting dist for 1st object
        var shortestDistance : Double
        var nearestStop : Stop = allStops.objectAtIndex(0) as Stop
        var requiredDict = NSMutableDictionary()
        
        var lat: Double = nearestStop.stop_lat as Double
        var lon: Double = nearestStop.stop_lon as Double
        var currentStopCoord :CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, lon)
        
        var sourceLocation = CLLocation(latitude: lat, longitude: lon)
        var currentLocation = CLLocation(latitude: coord.latitude, longitude: coord.longitude)
        shortestDistance = currentLocation.distanceFromLocation(sourceLocation)
        
        for item in allStops {
            let currentStop = item as Stop
            
            var lat: Double = currentStop.stop_lat as Double
            var lon: Double = currentStop.stop_lon as Double
            var currentStopCoord :CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, lon)
            
            var sourceLocation = CLLocation(latitude: lat, longitude: lon)
            var currentLocation = CLLocation(latitude: coord.latitude, longitude: coord.longitude)
            var totalDistance = currentLocation.distanceFromLocation(sourceLocation)
            if totalDistance < shortestDistance {
                shortestDistance = totalDistance
                nearestStop = currentStop as Stop
                
                requiredDict.setValue(nearestStop, forKey: "stop")
                requiredDict.setValue((totalDistance * 0.621371/1000), forKey: "stop_distance")            }
        }
        
        
        return requiredDict
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 44.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        var aView : UIView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 44))
        aView.backgroundColor = UIColor(red: (0.0/255.0), green: (122.0/255.0), blue: (255.0/255.0), alpha: 1.0)
        var aLabel : UILabel = UILabel(frame: aView.frame)
        aLabel.textAlignment = NSTextAlignment.Center
        aLabel.backgroundColor = UIColor.clearColor()
        aLabel.textColor = UIColor.whiteColor()
        aLabel.font = UIFont.systemFontOfSize(12.0)
        
        var nearestStationString : NSString = nearestStop.valueForKey("stop_name") as NSString
        var sectionString : String = String("Next service at \(nearestStationString)")
        aLabel.text = sectionString
        
        aView.addSubview(aLabel)
        return aView
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return latestSchedule.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 66.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell : HomeTableViewCell = tableView.dequeueReusableCellWithIdentifier("HomeTableViewCell") as HomeTableViewCell
        
        var currentScheduleDict : NSDictionary = latestSchedule.objectAtIndex(indexPath.row) as NSDictionary
        var currentSchedule : StopTime = currentScheduleDict.valueForKey("StopTime") as StopTime
        var timeDifference : NSTimeInterval = currentScheduleDict.valueForKey("time_diff") as NSTimeInterval
        if(timeDifference < 1) {
            cell.lbl_Time?.text = "now"
        }
        else if(timeDifference > 1 && timeDifference < 2) {
            cell.lbl_Time?.text = "1 min"
        }
        else {
            cell.lbl_Time?.text = NSString(format: "%d mins", Int(timeDifference))
        }
        cell.lbl_Time.layer.cornerRadius = 5
        var destinationString : NSString = dbManager.getDestinationForTripID(currentSchedule.trip_id)
        cell.lbl_TowardsStation.text = destinationString
        
        return cell
    }
}

