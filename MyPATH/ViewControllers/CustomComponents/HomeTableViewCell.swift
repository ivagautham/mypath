//
//  HomeTableViewCell.swift
//  MyPATH
//
//  Created by VA Gautham  on 10/16/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet var lbl_Time : UILabel!
    @IBOutlet var lbl_TowardsStation : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
