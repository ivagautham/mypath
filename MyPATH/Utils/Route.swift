//
//  Route.swift
//  MyPATH
//
//  Created by VA Gautham  on 10/17/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import Foundation

class Route: NSObject   {
    var route_id: NSNumber!
    var route_name: String!
    var route_desc: String!
}
