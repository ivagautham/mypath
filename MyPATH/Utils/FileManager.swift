//
//  FileManager.swift
//  MyPATH
//
//  Created by VA Gautham  on 9/22/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import Foundation

class FileManager {
    
    var allStopDetails = NSArray()
    var allShapeDetails = NSArray()
    var allServiceDetails = NSArray()
    var allScheduleDetails = NSArray()
    var allRouteDetails = NSArray()
    var allTripDetails = NSArray()
    
    func fetchDetailsFromFile(fileName:NSString) -> AnyObject {
        
        let bundle = NSBundle.mainBundle()
        let path = bundle.pathForResource(fileName, ofType: "json")
        var error:NSError?
        var data:NSData = NSData(contentsOfFile: path!)!
        let json:AnyObject = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error)!
        return json
    }
    
    func stopDetails() -> NSArray {
        if(allStopDetails.count == 0) {
            allStopDetails = fetchDetailsFromFile("stops") as NSArray
        }
        return allStopDetails
    }
    
    func shapeDetails() -> NSArray {
        if(allShapeDetails.count == 0) {
            allShapeDetails = fetchDetailsFromFile("shapes") as NSArray
        }
        return allShapeDetails
    }
    
    func scheduleDetails() -> NSArray {
        if(allScheduleDetails.count == 0) {
            allScheduleDetails = fetchDetailsFromFile("stop_times") as NSArray
        }
        return allScheduleDetails
    }
    
    
    func routeDetails() -> NSArray {
        if(allRouteDetails.count == 0) {
            allRouteDetails = fetchDetailsFromFile("routes") as NSArray
        }
        return allRouteDetails
    }
    
    func serviceDetails() -> NSArray {
        if(allServiceDetails.count == 0) {
            allServiceDetails = fetchDetailsFromFile("calendar") as NSArray
        }
        return allServiceDetails
    }
    
    func tripDetails() -> NSArray {
        if(allTripDetails.count == 0) {
            allTripDetails = fetchDetailsFromFile("trips") as NSArray
        }
        return  allTripDetails
    }
}