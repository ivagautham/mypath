//
//  StopTime.swift
//  MyPATH
//
//  Created by VA Gautham  on 10/17/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import Foundation

class StopTime: NSObject {
    var trip_id: String!
    var departure_time: String!
    var stop_id: NSNumber!
    var stop_sequence: NSNumber!
}
