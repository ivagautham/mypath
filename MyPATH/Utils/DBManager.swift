//
//  DBManager.swift
//  MyPATH
//
//  Created by VA Gautham  on 10/22/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import Foundation

class DBManager {
    
    var allStop = NSMutableArray()
    var allService = NSMutableArray()
    var allSchedule = NSMutableArray()
    var allRoute = NSMutableArray()
    var allTrip = NSMutableArray()
    
    let fileManager = FileManager()
    
    func prepareLoadStore() {
        
        var load: Bool = NSUserDefaults.standardUserDefaults().boolForKey("DataSetup")
        load = false
        if(load == false)
        {
            populateStopTable()
            populateServiceTable()
            populateStopTimeTable()
            populateTripTable()
            populateRouteTable()
            
            //saveUserDefaults()
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "DataSetup")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        else
        {
            loadUserDefaults()
        }
    }
    
    func saveUserDefaults()
    {
        NSUserDefaults.standardUserDefaults().setObject(allStop, forKey: "allStop")
        NSUserDefaults.standardUserDefaults().setObject(allRoute, forKey: "allRoute")
        NSUserDefaults.standardUserDefaults().setObject(allService, forKey: "allService")
        NSUserDefaults.standardUserDefaults().setObject(allSchedule, forKey: "allSchedule")
        NSUserDefaults.standardUserDefaults().setObject(allTrip, forKey: "allTrip")
        
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func loadUserDefaults()
    {
        allStop = NSMutableArray(array: (NSUserDefaults.standardUserDefaults().objectForKey("allStop") as NSMutableArray))
        allRoute = NSMutableArray(array: (NSUserDefaults.standardUserDefaults().objectForKey("allRoute") as NSMutableArray))
        allService = NSMutableArray(array: (NSUserDefaults.standardUserDefaults().objectForKey("allService") as NSMutableArray))
        allSchedule = NSMutableArray(array: (NSUserDefaults.standardUserDefaults().objectForKey("allSchedule") as NSMutableArray))
        allTrip = NSMutableArray(array: (NSUserDefaults.standardUserDefaults().objectForKey("allTrip") as NSMutableArray))
    }
    
    func populateStopTable() {
        
        var stopsArray : NSArray = fileManager.stopDetails()
        
        for item in stopsArray {
            let currentStop = item as NSMutableDictionary
            var lat: Double = currentStop["stop_lat"] as Double
            var lon: Double = currentStop["stop_lon"] as Double
            var stopid : NSNumber = currentStop["stop_id"] as NSNumber
            var stopname : NSString = currentStop["stop_name"] as NSString
            
            let newItem = Stop()
            
            newItem.stop_id = stopid
            newItem.stop_name = stopname
            newItem.stop_lat = lat
            newItem.stop_lon = lon
            
            allStop.addObject(newItem)
        }
    }
    
    func populateRouteTable() {
        
        var routeArray : NSArray = fileManager.routeDetails()
        
        for item in routeArray {
            let currentRoute = item as NSDictionary
            
            var routeid : NSNumber = currentRoute["route_id"] as NSNumber
            var routename : NSString = currentRoute["route_long_name"] as NSString
            var routedesc : NSString = currentRoute["route_desc"] as NSString
            
            let newItem = Route()
            
            newItem.route_id = routeid
            newItem.route_name = routename
            newItem.route_desc = routedesc
            
            allRoute.addObject(newItem)
            
        }
    }
    
    func populateServiceTable() {
        
        var serviceArray : NSArray = fileManager.serviceDetails()
        
        for item in serviceArray {
            let currentService = item as NSDictionary
            
            let newItem = Service()
            
            newItem.service_id = currentService["service_id"] as NSString
            newItem.service_sunday = (currentService["sunday"] as NSString).boolValue
            newItem.service_monday = (currentService["monday"] as NSString).boolValue
            newItem.service_tuesday = (currentService["tuesday"] as NSString).boolValue
            newItem.service_wednesday = (currentService["wednesday"] as NSString).boolValue
            newItem.service_thursday = (currentService["thursday"] as NSString).boolValue
            newItem.service_friday = (currentService["friday"] as NSString).boolValue
            newItem.service_sunday = (currentService["saturday"] as NSString).boolValue
            
            allService.addObject(newItem)
        }
        
    }
    
    func populateStopTimeTable() {
        
        var timeArray : NSArray = fileManager.scheduleDetails()
        
        for item in timeArray {
            let currentTime = item as NSDictionary
            
            let newItem = StopTime()
            
            newItem.stop_id = currentTime["stop_id"] as NSNumber
            newItem.trip_id = currentTime["trip_id"] as NSString
            newItem.stop_sequence = currentTime["stop_sequence"] as NSNumber
            newItem.departure_time = currentTime["departure_time"] as NSString
            
            allSchedule.addObject(newItem)
        }
    }
    
    func populateTripTable() {
        
        var tripArray : NSArray = fileManager.tripDetails()
        
        for item in tripArray {
            let currentTrip = item as NSDictionary
            
            let newItem = Trip()
            
            newItem.trip_id = currentTrip["trip_id"] as NSString
            newItem.service_id = currentTrip["service_id"] as NSString
            newItem.route_id = currentTrip["route_id"] as NSNumber
            newItem.trip_name = currentTrip["trip_headsign"] as NSString
            newItem.trip_direction = (currentTrip["direction_id"] as NSString).boolValue
            
            allTrip.addObject(newItem)
        }
    }
    
    
    func stopDetails() -> NSArray {
        return allStop
    }
    
    func scheduleDetails() -> NSArray {
        return allSchedule
    }
    
    func routeDetails() -> NSArray {
        return allRoute
    }
    
    func serviceDetails() -> NSArray {
        return allService
    }
    
    func tripDetails() -> NSArray {
        return  allTrip
    }
    
    
    func getScheduleForStopID(stopID: NSNumber) -> NSArray
    {
        var scheduleArray = scheduleDetails()
        var pred : NSPredicate = NSPredicate(format: "stop_id == %@", stopID)!
        var searchResults = scheduleArray.filteredArrayUsingPredicate(pred) as NSArray
        return searchResults
    }
    
    func checkIfLastStopfor(CurrentSchedule currentSchedule : StopTime) -> Bool {
        
        var scheduleArray = scheduleDetails()
        var pred : NSPredicate = NSPredicate(format: "trip_id == %@", currentSchedule.trip_id)!
        var searchResults = scheduleArray.filteredArrayUsingPredicate(pred) as NSArray
        
        //        var descriptor: NSSortDescriptor = NSSortDescriptor(key: "stop_sequence", ascending: false)
        //        var sortedSearchResults: NSArray = searchResults.sortedArrayUsingDescriptors([descriptor])
        
        var maxTrip : StopTime = searchResults.objectAtIndex(0) as StopTime
        
        if(searchResults.containsObject(currentSchedule)) {
            return true
        }
        
        return false
    }
    
    func getDestinationForTripID(tripID : NSString) -> NSString
    {
        var tripArray = tripDetails()
        var pred : NSPredicate = NSPredicate(format: "trip_id == %@", tripID)!
        var searchResults : NSArray = tripArray.filteredArrayUsingPredicate(pred) as NSArray
        if(searchResults.count > 0) {
            var currentTrip : Trip = searchResults.objectAtIndex(0) as Trip
            return currentTrip.trip_name
        }
        return NSString(string: "?")
    }
    
    func getAllTripIDforStopID(stopID: NSNumber) -> NSArray
    {
        var scheduleArray: NSArray = getScheduleForStopID(stopID as NSNumber) as NSArray
        var requiredArray: NSMutableArray = NSMutableArray()
        for item in scheduleArray {
            let currentStopTime = item as StopTime
            requiredArray.addObject(currentStopTime.trip_id)
        }
        return requiredArray
    }
    
    func getAllTripIDforRouteID(routeID: NSNumber) -> NSArray
    {
        var tripArray = tripDetails()
        var pred : NSPredicate = NSPredicate(format: "route_id == %@", routeID)!
        var searchResults = tripArray.filteredArrayUsingPredicate(pred) as NSArray
        
        var requiredArray: NSMutableArray = NSMutableArray()
        for item in searchResults {
            let currentTrip = item as Trip
            requiredArray.addObject(currentTrip.trip_id)
        }
        return requiredArray
    }
    
    func getAllRouteForStopID(stopID: NSNumber) -> NSArray
    {
        var stopTripIDArray = getAllTripIDforStopID(stopID) as NSArray
        
        var requiredArray: NSMutableArray = NSMutableArray()
        
        var routeArray = routeDetails()
        for item in routeArray {
            let currentRoute = item as Route
            var routeTripIDArray = getAllTripIDforRouteID(currentRoute.route_id) as NSArray
            
            var set1: NSSet = NSSet(array: stopTripIDArray) as NSSet
            var set2: NSSet = NSSet(array: routeTripIDArray) as NSSet
            if(set1.intersectsSet(set2)) {
                requiredArray.addObject(currentRoute)
            }
            else if(set2.intersectsSet(set1)) {
                requiredArray.addObject(currentRoute)
            }
        }
        return requiredArray
    }
    
    func getAllConnectionsForRouteArray(routeArray: NSArray) -> NSArray {
        var requiredArray: NSMutableArray = NSMutableArray()
        var allStopArray : NSArray = stopDetails() as NSArray
        
        for item in routeArray {
            let currentRoute = item as Route
            var routeTripIDArray = getAllTripIDforRouteID(currentRoute.route_id) as NSArray
            for item in allStopArray {
                let currentStop = item as Stop
                var stopTripIDArray = getAllTripIDforStopID(currentStop.stop_id) as NSArray
                
                var set1: NSSet = NSSet(array: stopTripIDArray) as NSSet
                var set2: NSSet = NSSet(array: routeTripIDArray) as NSSet
                if(set1.intersectsSet(set2) && (requiredArray.containsObject(currentStop) == false))  {
                    requiredArray.addObject(currentStop)
                }
                else if(set2.intersectsSet(set1) && (requiredArray.containsObject(currentStop) == false)) {
                    requiredArray.addObject(currentStop)
                }

            }
        }
        return requiredArray
    }
}