//
//  Stop.swift
//  MyPATH
//
//  Created by VA Gautham  on 10/17/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import Foundation

class Stop: NSObject {
    var stop_id: NSNumber!
    var stop_name: String!
    var stop_lat: NSNumber!
    var stop_lon: NSNumber!
}
