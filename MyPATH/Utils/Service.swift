//
//  Service.swift
//  MyPATH
//
//  Created by VA Gautham  on 10/17/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import Foundation

class Service: NSObject {
    var service_id: String!
    var service_sunday: NSNumber!
    var service_monday: NSNumber!
    var service_tuesday: NSNumber!
    var service_wednesday: NSNumber!
    var service_thursday: NSNumber!
    var service_friday: NSNumber!
    var service_saturday: NSNumber!
}
