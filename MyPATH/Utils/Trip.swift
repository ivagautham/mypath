//
//  Trip.swift
//  MyPATH
//
//  Created by VA Gautham  on 10/17/14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

import Foundation

class Trip: NSObject {
    var route_id: NSNumber!
    var service_id: String!
    var trip_id: String!
    var trip_name: String!
    var trip_direction: NSNumber!
}
